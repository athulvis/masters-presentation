\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{{Introduction to Modern Cosmology}}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{General Theory of Relativity}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Red shift}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Hubble's Law}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{Friedmann Cosmology}{8}{0}{1}
\beamer@sectionintoc {2}{Standard $\Lambda $CDM Model}{15}{0}{2}
\beamer@subsubsectionintoc {2}{0}{1}{Dark Matter and Dark Energy}{17}{0}{2}
\beamer@sectionintoc {3}{Analysis of $\Lambda $CDM Model}{18}{0}{3}
\beamer@subsectionintoc {3}{1}{Scale Factor}{18}{0}{3}
\beamer@subsectionintoc {3}{2}{Hubble Parameter}{20}{0}{3}
\beamer@subsectionintoc {3}{3}{Deceleration Parameter}{23}{0}{3}
\beamer@subsectionintoc {3}{4}{Distance Moduli vs Redshift}{26}{0}{3}
